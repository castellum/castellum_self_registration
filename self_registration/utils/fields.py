# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.db import models

from phonenumber_field.modelfields import PhoneNumberField as _PhoneNumberField

from .forms import DateField as DateFormField
from .forms import PhoneNumberField as PhoneNumberFormField


class DateField(models.DateField):
    def formfield(self, **kwargs):
        kwargs.setdefault('form_class', DateFormField)
        return super().formfield(**kwargs)


class PhoneNumberField(_PhoneNumberField):
    def formfield(self, **kwargs):
        kwargs.setdefault('form_class', PhoneNumberFormField)
        return super().formfield(**kwargs)
