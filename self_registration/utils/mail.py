# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.core.mail import EmailMessage
from django.urls import reverse


def send_confirmation_mail(selfregistered_subject, request):
    subject = settings.SELF_REGISTRATION_CONFIRMATION_MAIL_SUBJECT
    from_email = None
    to_email = selfregistered_subject.email
    confirmation_link = request.build_absolute_uri(
        reverse('subject-confirm', args=[selfregistered_subject.verification_token])
    )
    delete_link = request.build_absolute_uri(
        reverse('subject-delete', args=[selfregistered_subject.verification_token])
    )
    email_body = settings.SELF_REGISTRATION_CONFIRMATION_MAIL_BODY.format(
        name=selfregistered_subject.full_name,
        confirmation_link=confirmation_link,
        delete_link=delete_link,
        self_registration_mail=settings.SELF_REGISTRATION_MAIL,
    )
    email = EmailMessage(
        subject, email_body, from_email, to=[to_email]
    )
    return email.send(fail_silently=True)
