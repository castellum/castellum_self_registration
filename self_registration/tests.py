from django.conf import settings
from django.test import TestCase
from model_bakery import baker

from .main.models import SelfRegisteredSubject


class TestSelfRegistrationApiView(TestCase):
    def setUp(self):
        self.client.defaults['HTTP_AUTHORIZATION'] = 'token ' + settings.API_TOKEN

    def test_get(self):
        url = '/api/subjects/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_confirmed_subject_in_response(self):
        baker.make(
            SelfRegisteredSubject,
            email='test@example.com',
            confirmed=True
        )
        url = '/api/subjects/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"test@example.com", response.content)

    def test_unconfirmed_subject_not_in_response(self):
        baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/api/subjects/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"subjects": []}')

    def test_delete_deletes_confirmed_subjects(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=True
        )
        url = '/api/subjects/?id={}'.format(subject.pk)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertFalse(SelfRegisteredSubject.objects.exists())

    def test_delete_doesnt_delete_unconfirmed_subjects(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/api/subjects/?id={}'.format(subject.pk)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 404)
        self.assertTrue(SelfRegisteredSubject.objects.filter(pk=subject.pk).exists())


class TestSelfRegistrationDeleteView(TestCase):
    def test_get(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/delete/{}/'.format(subject.verification_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_deletion(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/delete/{}/'.format(subject.verification_token)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(SelfRegisteredSubject.objects.filter(pk=subject.pk).exists())

    def test_deletion_only_works_for_unconfirmed_subjects(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=True
        )
        url = '/delete/{}/'.format(subject.verification_token)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 404)
        self.assertTrue(SelfRegisteredSubject.objects.filter(pk=subject.pk).exists())


class TestSelfRegistrationConfirmView(TestCase):
    def test_get(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/confirm/{}/'.format(subject.verification_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            SelfRegisteredSubject.objects.filter(pk=subject.pk, confirmed=True).exists()
        )

    def test_confirmation_deletes_token(self):
        subject = baker.make(
            SelfRegisteredSubject,
            confirmed=False
        )
        url = '/confirm/{}/'.format(subject.verification_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            SelfRegisteredSubject.objects.filter(pk=subject.pk, verification_token=None).exists()
        )
