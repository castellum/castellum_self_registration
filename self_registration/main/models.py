# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _

from self_registration.utils.fields import DateField
from self_registration.utils.fields import PhoneNumberField


class SelfRegisteredSubject(models.Model):
    GENDER = [
        ("f", _("female")),
        ("m", _("male")),
        ("*", _("diverse")),
    ]

    first_name = models.CharField(_('First name'), max_length=64)
    last_name = models.CharField(_('Last name'), max_length=64)
    gender = models.CharField(_('Gender'), max_length=1, choices=GENDER, blank=True)
    email = models.EmailField(_('Email'), max_length=128)
    phone_number = PhoneNumberField(_('Phone number'), max_length=32, blank=True)
    date_of_birth = DateField(_('Date of birth'), blank=True, null=True)
    confirmed = models.BooleanField(_('Confirmed'), default=False)
    verification_token = models.UUIDField(_('Token'), default=uuid.uuid4, blank=True, null=True)
    created_at = models.DateField(_('Created at'), auto_now_add=True)

    @property
    def full_name(self):
        return " ".join(filter(None, [self.first_name, self.last_name]))
