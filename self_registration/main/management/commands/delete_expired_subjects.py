# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from self_registration.main.models import SelfRegisteredSubject


def delete_expired_subjects():
    return SelfRegisteredSubject.objects.filter(
        confirmed=False,
        created_at__lte=timezone.now()-settings.SUBJECT_EXPIRATION_PERIOD
    ).delete()


class Command(BaseCommand):
    help = 'Delete expired self-registered subjects.'

    def handle(self, *args, **options):
        count, _ = delete_expired_subjects()
        if options['verbosity'] > 0:
            self.stdout.write(
                "{count} self-registered subjects were deleted, as they had not been confirmed "
                "within {period} days.".format(
                    count=count,
                    period=settings.SUBJECT_EXPIRATION_PERIOD.days
                )
            )
