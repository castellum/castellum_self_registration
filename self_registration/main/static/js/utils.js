document.querySelectorAll('[type="date"]').forEach(function(e) {
    if (e.type !== 'date') {
        new Datepicker(e, {
            format: 'yyyy-mm-dd',
            language: document.documentElement.lang,
            autoclose: true,
            calendarWeeks: true,
            daysOfWeekHighlighted: [0, 6],
            todayHighlight: true,
        });
    }
});
