# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.


from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import TemplateView
from django.views.generic import View

from .models import SelfRegisteredSubject
from ..utils.mail import send_confirmation_mail


class SelfRegisteredSubjectCreateView(CreateView):
    model = SelfRegisteredSubject
    fields = ['first_name', 'last_name', 'gender', 'date_of_birth', 'email', 'phone_number']

    def get_success_url(self):
        return reverse('subject-created')

    def form_valid(self, form, *args):
        if not send_confirmation_mail(form.instance, self.request):
            messages.error(
                self.request,
                _('There have been errors while trying to send the confirmation mail.'),
            )
            return super().form_invalid(form, *args)
        form.save()
        return self.response_class(self.request, 'main/selfregisteredsubject_created.html')


class SelfRegisteredSubjectConfirmView(TemplateView):
    template_name = 'main/selfregisteredsubject_confirmed.html'

    def get(self, request, *args, **kwargs):
        subject = get_object_or_404(
            SelfRegisteredSubject, verification_token=self.kwargs.get('token'), confirmed=False
        )
        subject.confirmed = True
        subject.verification_token = None
        subject.save()
        return super().get(request, *args, **kwargs)


class SelfRegisteredSubjectDeleteView(DeleteView):
    model = SelfRegisteredSubject

    def get_object(self):
        return get_object_or_404(
            SelfRegisteredSubject, verification_token=self.kwargs.get('token'), confirmed=False
        )

    def delete(self, request, *args, **kwargs):
        subject = self.get_object()
        subject.delete()
        return self.response_class(request, 'main/selfregisteredsubject_deleted.html')


@method_decorator(csrf_exempt, 'dispatch')
class SelfRegistrationApiView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.headers.get('Authorization') != 'token ' + settings.API_TOKEN:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        subjects = SelfRegisteredSubject.objects.filter(confirmed=True)
        data = []
        for subject in subjects:
            data.append({
                'first_name': subject.first_name,
                'last_name': subject.last_name,
                'gender': subject.gender,
                'email': subject.email,
                'phone_number': str(subject.phone_number),
                'date_of_birth': subject.date_of_birth,
                'delete_url': request.build_absolute_uri(request.path) + '?id={}'.format(subject.pk)
            })
        return JsonResponse({'subjects': data})

    def delete(self, request, *args, **kwargs):
        subject = get_object_or_404(SelfRegisteredSubject, confirmed=True, pk=request.GET.get('id'))
        subject.delete()
        return HttpResponse(status=204)
