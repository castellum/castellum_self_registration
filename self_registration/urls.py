# (c) 2021 MPIB <https://www.mpib-berlin.mpg.de/>,
#
# This file is part of castellum-self-registration.
#
# castellum-self-registration is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.contrib import admin
from django.urls import path

from .main.views import SelfRegisteredSubjectConfirmView
from .main.views import SelfRegisteredSubjectCreateView
from .main.views import SelfRegisteredSubjectDeleteView
from .main.views import SelfRegistrationApiView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', SelfRegisteredSubjectCreateView.as_view(), name='subject-create'),
    path(
        'confirm/<uuid:token>/',
        SelfRegisteredSubjectConfirmView.as_view(),
        name='subject-confirm',
    ),
    path(
        'delete/<uuid:token>/',
        SelfRegisteredSubjectDeleteView.as_view(),
        name='subject-delete',
    ),
    path('api/subjects/', SelfRegistrationApiView.as_view(), name='api-subjects'),
]
