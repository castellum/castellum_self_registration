FROM node:14.17.6
COPY package.json .
RUN npm install --production

FROM alpine:3.13.6

ENV PYTHONUNBUFFERED 1
ENV PKGS gettext python3 py3-pip py3-wheel uwsgi uwsgi-python py3-psycopg2 py3-pyldap

RUN adduser -D -g '' uwsgi

RUN apk update && apk add $PKGS

WORKDIR code/
COPY setup.cfg setup.py uwsgi.ini LICENSE ./
COPY --from=0 node_modules/ node_modules

RUN pip3 install -e . && \
    pip3 install django-auth-ldap

COPY self_registration/ self_registration

RUN django-admin collectstatic --no-input --settings=self_registration.settings.development && \
    django-admin compilemessages -l de

CMD uwsgi uwsgi.ini --die-on-term
