VIRTUAL_ENV ?= .venv
MANAGEPY = $(VIRTUAL_ENV)/bin/python manage.py

.PHONY: all
all: install run

.PHONY: run
run:
	$(MANAGEPY) runserver 8002

.PHONY: install
install:
	if [ ! -d "$(VIRTUAL_ENV)" ]; then python3 -m venv "$(VIRTUAL_ENV)"; fi
	$(VIRTUAL_ENV)/bin/pip install -e .[test]
	npm install
	$(MANAGEPY) migrate
	$(MANAGEPY) shell -c "from django.contrib.auth.models import User; User.objects.filter(username='admin').exists() or User.objects.create_superuser('admin', 'admin@example.com', 'password')"
	$(MANAGEPY) compilemessages -l de

.PHONY: test
test:
	$(VIRTUAL_ENV)/bin/flake8
	$(MANAGEPY) test

.PHONY: makemessages
makemessages:
	$(MANAGEPY) makemessages -l de -d django
	$(MANAGEPY) makemessages -l de -d djangojs
